﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public GameObject player;
    Vector3 offset;
    // Start is called before the first frame update
    void Start()
    {
        offset = transform.position - player.transform.position; //sets the difference between the camera and player's position 
    }

    // Update is called once per frame
    void LateUpdate()
    {
        transform.position = player.transform.position + offset; //moves camera to position relative to the player's transform
    }
}
