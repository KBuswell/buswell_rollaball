﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    //Set up variables
    public float speed;
    public Text countText;
    public Text winText;

    Rigidbody rb;
    int count;

    // Start is called before the first frame update
    void Start()
    {
        //set initial variable values
        count = 0;
        rb = GetComponent<Rigidbody>();
        setCountText();
        winText.text = "";
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        //get movement input
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        //move the player
        Vector3 Movement = new Vector3(moveHorizontal, 0f, moveVertical);
        rb.AddForce(Movement * speed);
        //test if all the pickups on the bottom floor have been collected
        if (count >= 8)
        {
            // if so, invert gravity
            Physics.gravity = new Vector3(0, 1, 0);
        }
    }
    void OnTriggerEnter(Collider other)
    {
        //test if the trigger collided with is a pickup
        if (other.gameObject.CompareTag("Pickup"))
        {
            //remove the collectible and increment count
            other.gameObject.SetActive(false);
            count++;
            setCountText();
        }
        //Test if the trigger collided with is the portal
        if (other.gameObject.CompareTag("Portal"))
        {             
            transform.position = new Vector3(0, 9, 0);
        }
    }
    void setCountText()
    {
        //update the count UI element with the new number of collected items
        countText.text = "Count: " + count.ToString();
        //display the win message if the player has collected all of the pickups
        if (count >= 16)
        {
            winText.text = "You Win!";
        }
    }
}
